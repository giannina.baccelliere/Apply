export interface News {
    hits: Hit[]
  }
  export interface Hit {
    created_at: string
    title: any
    url: any
  }